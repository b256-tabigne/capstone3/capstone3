import React from "react";
import ReactDOM from "react-dom";
import { Link, NavLink, useParams } from 'react-router-dom';
import { useState, useContext, useEffect } from 'react';
import UserContext from '../userContext';
import { Button, Card, Col, Row, Container } from 'react-bootstrap'
import { getUserData } from '../sendHelp/Helper';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';

export default function LandingPage() {

  const [userLogin, setUserLogin] = useState(null);
  const navigate = useNavigate()

  useEffect(() => {
    const userId = getUserData();
    if (userId === null || userId === undefined) {
      Swal.fire({
        title: 'Access failed',
        icon: 'error',
        text: 'You do not have access to this page.',
      });
      navigate('/login');
    } else {
      console.log(userId)
      fetch(`http://localhost:3001/users/${userId.id}`)
        .then((res) => res.json())
        .then((data) => {
          setUserLogin(data);
        });
    }
  }, [navigate]);

  return (
    <Container style={{marginTop: "50px", margin: "50px auto", border: "5px solid", borderRadius:"32px", width: "fit-content", padding: "48px", backgroundColor: "white"}}>
    <h1 className="text-center">Welcome!</h1>
      <Row className="pr-5 pb-5 pl-5 text-center text-dark">
        <Col className="m-3">
          <h1>My User:</h1>
            <Card style={{ width: '18rem' }}>
                <Card.Body>
                <Card.Title>
                  {userLogin?.email}
                </Card.Title>
                <Card.Text>Name: 
                  {userLogin?.firstName} {userLogin?.lastName}
                </Card.Text>
                <Card.Text>Balance: 
                  {userLogin?.balance}
                </Card.Text>
                  <Button variant="dark" href="/balance">Edit</Button>
              </Card.Body>
            </Card>
        </Col>
      </Row>
    </Container>
  );
}