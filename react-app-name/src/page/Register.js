import React, { useState } from 'react';
import Swal from 'sweetalert2';
import { useParams, Link, useNavigate } from 'react-router-dom';

export default function Register() {
  const [email, setEmail] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [password, setPassword] = useState('');

  const navigate = useNavigate()

  const handleSubmitUser = (event) => {
    event.preventDefault();

    

    fetch('REACT_APP_API_URL=http://localhost:3001/users/register', {
				method: "POST",
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email,
					firstName,
					lastName,
					password
				})
			})

			.then(res => {
			})
			.then(req => {
				if(req !== true) {
				Swal.fire({
					title: "Successfully Created!",
					icon: "success",
					text: "Welcome, Shop all you want!"
				})

				navigate("/home");
			} else {
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again"
				})
			}
		})
		
  }

  return (
  	<section>
  		<div align="center" style={{marginTop: "50px", margin: "50px auto", border: "5px solid", borderRadius:"32px", width: "fit-content", padding: "48px", backgroundColor: "white"}}>	
		    <form onSubmit={handleSubmitUser}>
		      <label>
		        Email:
		      </label>
		      <br />
		        <input
		          type="email"
		          value={email}
		          onChange={(event) => setEmail(event.target.value)}
		          required
		        />
		      <br />
		      <br />
		      <label>
		        First Name:
		      </label>
		      <br />
		        <input
		          type="text"
		          value={firstName}
		          onChange={(event) => setFirstName(event.target.value)}
		          required
		        />
		      
		      <br />
		      <br />
		      <label>
		        Last Name:
		      </label>
		      <br />
		        <input
		          type="text"
		          value={lastName}
		          onChange={(event) => setLastName(event.target.value)}
		          required
		        />
		      
		      <br />
		      <br />
		      <label>
		        Password:
		      </label>
		      <br />
		        <input
		          type="password"
		          value={password}
		          onChange={(event) => setPassword(event.target.value)}
		          required
		        />
		      
		      <br />
		      <br />
		      <button type="submit">Register</button>
		    </form>
		</div>
	</section>
  );
}