import { Container, Navbar, Nav, NavDropdown } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../userContext';
import Logo from '../img/logo.jpg';
import { getUserData } from '../sendHelp/Helper';

export default function AppNavBar() {

    const user = getUserData()

    console.log(user)

  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Container>
        <Navbar.Brand href="/">
            <img src={Logo}
              width="50"
              height="50"
              className="d-inline-block align-top"
              style={{ borderRadius: '50%' }}
            />
            </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
              {user && user.role === 'admin' ? (
                <>
                <Nav.Link href="/admindashboard">Admin DashBoard</Nav.Link>
                <NavDropdown title="Admin" id="collasible-nav-dropdown">
                  <NavDropdown.Item href="/product">Products</NavDropdown.Item>
                  <NavDropdown.Item href="/create">Create product</NavDropdown.Item>
                </NavDropdown>
                </>
                ) : (
              <>
                <Nav.Link href="/home">Home</Nav.Link>
                <Nav.Link href="/product">Product</Nav.Link>
              </>
            )}
          </Nav>
          <Nav>
            <Nav.Link href="/cart">Cart</Nav.Link>
            {user && user.id !== null ? (
              <Nav.Link as={NavLink} to="/logout">
                Logout
              </Nav.Link>
                ) : (
              <>
                <Nav.Link as={NavLink} to="/login">
                  Login
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register">
                  Register
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}