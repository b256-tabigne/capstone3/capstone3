import HomePage from '../components/HomePage';

export default function Error() {

    const data = {
        title: "404 - Not found",
        content: "The page you are looking for cannot be found",
        destination: "/home",
        label: "Back home"
    }
    
    return (
        <HomePage data={data}/>
    )
}
