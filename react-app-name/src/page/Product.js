import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';
import { getUserData } from '../sendHelp/Helper';

export default function AddProduct() {

	const user = getUserData()

	const { userId } = useParams()

	const navigate = useNavigate()

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [quantity, setQuantity] = useState(1)

  const handleSubmitProduct = (event) => {
    event.preventDefault();
    console.log(user)
    fetch('REACT_APP_API_URL=http://localhost:3001/products/createProduct', {
				method: "POST",
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					name,
					description,
					price,
					quantity
				})
			})
			.then(res => {
				console.log(res)
				if(res.data === null){
					return false
			}
			Swal.fire({
					title: "You've input a new Product!",
					icon: "success",
					text: "Successfully Added"
				})
				navigate("/create")
			})
			.catch(err => {
				console.log(err)
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again"
				})
		})	
  }

  if (user.role !== 'admin') {
    return <p className="text-light">You do not have access to this page.</p>;
  }
   

  return (
  	<section>
  		<div align="center" style={{marginTop: "50px", margin: "50px auto", border: "5px solid", borderRadius:"32px", width: "fit-content", padding: "48px", backgroundColor: "white"}}>	
		    <form onSubmit={handleSubmitProduct}>
		      <label>
		        Product Name:
		      </label>
		      <br />
		        <input
		          type="text"
		          value={name}
		          onChange={(event) => setName(event.target.value)}
		          required
		        />
		      <br />
		      <br />
		      <label>
		        Description:
		      </label>
		      <br />
		        <input
		          type="text"
		          value={description}
		          onChange={(event) => setDescription(event.target.value)}
		          required
		        />

		      <br />
		      <br />
		      <label>
		        Price:
		      </label>
		      <br />
		        <input
		          type="number"
		          value={price}
		          onChange={(event) => setPrice(event.target.value)}
		          required
		        /> 

		      <br />
		      <br />
		      <label>
		        quantity:
		      </label>
		      <br />
		        <input
		          type="number"
		          value={quantity}
		          onChange={(event) => setQuantity(event.target.value)}
		          required
		        />

		      <br />
		      <br />
		      <button type="submit">Add Product</button>
		    </form>
		</div>
	</section>
  );
}