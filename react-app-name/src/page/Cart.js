import React, { useEffect, useState } from 'react';

const CartPage = () => {
  const [cartItems, setCartItems] = useState([]);

  useEffect(() => {
    fetch(`REACT_APP_API_URL=http://localhost:3001/cart/cart`)
      .then((res) => res.json())
      .then((data) => {
        setCartItems(data);
        console.log(data)
      })
      .catch((error) => {
        console.error('Error retrieving cart items:', error);
      });
  }, []);

  return (
    <div className="text-light">
      <h1>Cart</h1>
      {cartItems.length === 0 ? (
        <p>Your cart is empty.</p>
      ) : (
        <ul>
          {cartItems.map((item) => (
            <li key={item._id}>
              Product ID: {item.productId}, User ID: {item.userId}
            </li>
          ))}
        </ul>
      )}<button href="/order">Buy Order</button>
    </div>
  );
};

export default CartPage;