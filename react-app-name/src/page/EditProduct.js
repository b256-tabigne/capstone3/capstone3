import { useState, useEffect, useContext } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useParams, Link, useNavigate } from 'react-router-dom';
import { getUserData } from '../sendHelp/Helper';

export default function EditProduct() {

  const navigate = useNavigate();
  const { productId } = useParams();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [quantity, setQuantity] = useState('');
  const [status, setStatus] = useState(false);

  useEffect(() => {

   const user = getUserData();
    if (user === null || user === undefined){
    fetch(`http://localhost:3001/products/${productId}`)
      .then(res => res.json())
      .then(data => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setQuantity(data.quantity);
        setStatus(data.status);
      });
     } console.log(productId)
  }, [productId]);

  const edit = () => {
    fetch(`http://localhost:3001/products/editProduct`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        productId: productId
      })
    })
      .then(res => res.json())
      .then(data => {

        if (data.success) {
          Swal.fire({
            title: 'Successfully Edited!',
            icon: 'success',
            text: 'You have successfully edited the product.'
          });

          navigate('/product');
        }
      });
  };

        return (
            <form /*onSubmit={handleEditProductSubmit}*/>
                <section style={{marginTop: "50px", margin: "50px auto", border: "5px solid", borderRadius:"32px", width: "fit-content", padding: "48px", backgroundColor: "white"}}>
                    <div className="text-center">
                        <p>Name</p>
                        <input
                          type="text"
                          value={name}
                          onChange={(event) => setName(event.target.value)}
                          required
                        />
                        <br/>
                        <p>Description</p>
                        <input
                          type="text"
                          value={description}
                          onChange={(event) => setDescription(event.target.value)}
                          required
                        />
                        <br/>
                        <p>Price</p>
                        <input
                          type="number"
                          value={price}
                          onChange={(event) => setPrice(event.target.value)}
                          required
                        />
                        <br/>
                        <p>Quantity</p>
                        <input
                          type="number"
                          value={quantity}
                          onChange={(event) => setQuantity(event.target.value)}
                          required
                        />
                        <br/>
                        <p>Status</p>
                        <input
                          type="checkbox"
                          value={status}
                          onChange={(event) => setStatus(event.target.value)}
                        />
                        <br/>
                        <br/>
                        <Button variant="dark" type="submit" block onClick={() => edit(productId)}>Edit</Button>
                    </div>
                </section>
            </form>
        )
    
}
