import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';
import { getUserData } from '../sendHelp/Helper';

export default function AddProduct() {

  const user = getUserData()

  const navigate = useNavigate()

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [quantity, setQuantity] = useState(1)

  const handleSubmitProduct = (event) => {
    event.preventDefault();
    console.log(user)
    fetch('http://localhost:3001/products/createProduct', {
        method: "POST",
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
          name,
          description,
          price,
          quantity
        })
      })
      .then(res => {
        console.log(res)
        if(res.data === null){
          return false
      }
      Swal.fire({
          title: "You've input a new Product!",
          icon: "success",
          text: "Successfully Added"
        })
        navigate("/create")
      })
      .catch(err => {
        console.log(err)
        Swal.fire({
          title: "Something went wrong!",
          icon: "error",
          text: "Please try again"
        })
    })  
  }

  if (user.role !== 'admin') {
    return <p className="text-light">You do not have access to this page.</p>;
  }
   
  

  return (
    <section>
      <div align="center" style={{marginTop: "50px", margin: "50px auto", border: "5px solid", borderRadius:"32px", width: "fit-content", padding: "48px", backgroundColor: "white"}}>  
        <form onSubmit={handleSubmitProduct}>
          <label>
            Product Name:
          </label>
          <br />
            <input
              type="text"
              value={name}
              onChange={(event) => setName(event.target.value)}
              required
            />
          <br />
          <br />
          <label>
            Description:
          </label>
          <br />
            <input
              type="text"
              value={description}
              onChange={(event) => setDescription(event.target.value)}
              required
            />

          <br />
          <br />
          <label>
            Price:
          </label>
          <br />
            <input
              type="number"
              value={price}
              onChange={(event) => setPrice(event.target.value)}
              required
            /> 

          <br />
          <br />
          <label>
            quantity:
          </label>
          <br />
            <input
              type="number"
              value={quantity}
              onChange={(event) => setQuantity(event.target.value)}
              required
            />

          <br />
          <br />
          <button type="submit">Add Product</button>
        </form>
    </div>
    <br/>
  </section>
  );
}

export default function Product() {
  const user = getUserData();
  const [products, setProducts] = useState([]);

  let endpoint = "allActiveProducts";
  if (user.role === "admin") {
    endpoint = "allProducts";
  }

  return (
    <section>
      <div>
        <Container>
          <Row className="p-5 text-center text-dark">
              <Col className="mx-auto" xs={12} md={4} lg={3}>
                <Card style={{ width: '18rem' }}>
                  <Card.Img variant="center" className="rounded mx-auto d-block" alt="Edit" />
                  <Card.Body>
                    <Card.Title>{product.name}</Card.Title>
                    <Card.Text>Description: {product.description}</Card.Text>
                    <Card.Text>Price: ₱{product.price}</Card.Text>
                    <Card.Text>Quantity: {product.quantity}</Card.Text>
                    {user.role !== 'admin' ? (
                      <Button variant="dark" href="/cart">
                        Buy
                      </Button>
                    ) : (
                      <>
                        <Card.Text>Status: {product.status ? 'Active' : 'Inactive'}</Card.Text>
                        <Button variant="dark" href="/editProduct" onSubmit={Product}>Edit</Button>
                      </>
                    )}
                  </Card.Body>
                </Card>
              </Col>
          </Row>
        </Container>
      </div>
    </section>
    )
}
