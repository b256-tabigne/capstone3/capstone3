import './App.css';
import AppNavBar from './components/AppNavBar';
import { Container } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import UserContext from './userContext';
import { UserProvider } from './userContext';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Home from './page/Home';
import NotFound from './page/NotFound';
import Register from './page/Register';
import LandingPage from './page/LandingPage';
import Login from './page/Login';
import Logout from './page/Logout';
import Product from './page/Product';
import ProductList from './page/ProductList';
import Balance from './page/Balance';
import EditProduct from './page/EditProduct';
import Cart from './page/Cart';
import Order from './page/Order';
import AdminDashboard from './page/AdminDashboard';


function App() {


	const [user, setUser] = useState(null);

	return(
	<UserContext.Provider value={{ user, setUser }}>
	  <Router>
		<AppNavBar />
		<Container>
		  <Routes>
			<Route path="/" element={<LandingPage />} />
			<Route path="/register" element={<Register />} />
			<Route path="/adminDashboard" element={<AdminDashboard />} />
			<Route path="/login" element={<Login />} />
			<Route path="/home" element={<Home />} />
			<Route path="/create" element={<Product />} />
			<Route path="/cart" element={<Cart />} />
			<Route path="/editProduct" element={<EditProduct />} />
			<Route path="/order" element={<Order />} />
			<Route path="/product" element={<ProductList />} />
			<Route path="/logout" element={<Logout />} />
			<Route path="/balance" element={<Balance />} />
			<Route path="*" element={<NotFound />} />
		  </Routes>
		</Container>
	  </Router>
	</UserContext.Provider>  
 )
}

export default App;
