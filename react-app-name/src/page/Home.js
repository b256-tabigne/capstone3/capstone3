import HomePage from '../components/HomePage';
import { useState, useEffect } from 'react';
import UserContext from '../userContext';
import { useContext } from 'react';

export default function Home() {

    const { user } = useContext(UserContext)
    console.log(user)
    const data = {
        title: "Welcome to Teh Creatives!",
        content: "Teh Creatives offers high-quality and affordable layouts for your brand.",
        destination: "/product",
        label: "Shop now"
    }

    return (
        <>
            <HomePage data={data}/>
        </>
    )
}
