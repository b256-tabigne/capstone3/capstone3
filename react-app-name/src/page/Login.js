import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../userContext';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';
import { setUserData } from '../sendHelp/Helper';

export default function Login() {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false)
    const navigate = useNavigate()

    useEffect (() => {
      if(email !== '' && password !== '') {

        setIsActive(true)
      } else {

        setIsActive(false)

      }
    }, [email, password])
    function loginUser(e) {

      e.preventDefault();
      fetch('http://localhost:3001/users/login', {
        method: "POST",
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: email,
          password: password
        })
      })
      .then(res => res.json())
      .then(data => {

        console.log(data)

        if (typeof data.access !== "undefined") {

          localStorage.setItem('token', data.access)
          getProfile(data.access)
        } 
        else {
          Swal.fire({
            title: "Login failed",
            icon: "error",
            text: "Check your login deatils and try again!"
          })

        }

      })
      setEmail('');
      setPassword('');

    }

    const getProfile = (token) => {

      fetch('http://localhost:3001/users/details', {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => {

        if(data.role === "user"){
            Swal.fire({
                title: "Login Successful",
                icon: "success",
                text: "Welcome to TehCreatives!"
              })
              navigate("/product")}
        else if (data.role === "admin") {
          Swal.fire({
            title: "Welcome Admin",
            icon: "success",
            text: "Access Granted"
          })
          navigate("/home")
        }
        console.log(data)
        const userData = {
        id: data._id,
        role: data.role
        };

        setUserData(userData)
      })
    }

    return (
      <Form onSubmit={e => loginUser(e)}>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
          </Form.Group>
              <Button variant="primary" type="submit" id="submitBtn">Submit</Button> 
        </Form>
    )
}