import { Navigate } from 'react-router-dom';
import { deleteUserData } from '../sendHelp/Helper';

export default function Logout() {

	deleteUserData()

	return(

		<Navigate to="/home" />

	)
}
