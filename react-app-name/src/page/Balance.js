import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { getUserData } from '../sendHelp/Helper';

export default function Balance() {
  const [balance, setBalance] = useState('')

const handleBalanceSubmit = (event) => {
  event.preventDefault();

  if (!balance) {
    Swal.fire({
      title: 'Error',
      icon: 'error',
      text: 'Please enter a balance value.',
    });
    return;
  }

  const userId = getUserData()
  fetch(`REACT_APP_API_URL=http://localhost:3001/users/balance/${userId.id}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      balance: parseFloat(balance),
    }),
  })
    .then((res) => res.json())
    .then((data) => {
    })
    .catch((error) => {
      console.error('Error updating balance:', error);
    });

  setBalance('');
};

        return (
            <form onSubmit={handleBalanceSubmit}>
                <section style={{marginTop: "50px", margin: "50px auto", border: "5px solid", borderRadius:"32px", width: "fit-content", padding: "48px", backgroundColor: "white"}}>
                    <div className="text-center">
                        <h1>Balance</h1>
                        <input
                          type="number"
                          value={balance}
                          onChange={(event) => setBalance(event.target.value)}
                          required
                        />
                        <Button variant="dark" type="submit">Add</Button>
                    </div>
                </section>
            </form>
        )
    
}
