import { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import UserContext from '../userContext';
import { Button, Card, Col, Row, Container } from 'react-bootstrap';
import { addToCart } from '../api/cart';
import { getUserData } from '../sendHelp/Helper';

export default function Product() {
  const user = getUserData();
  const [products, setProducts] = useState([]);

  let endpoint = "allActiveProducts";
  if (user.role === "admin") {
    endpoint = "allProducts";
  }

  useEffect(() => {
    fetch(`http://localhost:3001/products/${endpoint}`)
      .then(res => res.json())
      .then(data => {
        setProducts(data);
      });
  }, [endpoint]);

  const addToCart = async (productId, userId) => {
    try {
      const response = await fetch('http://localhost:3001/cart/add', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          productId: productId,
          userId: userId,
        }),
      });

      const data = await response.json();
      return data.success; 
    } catch (error) {
      console.error('Error adding item to cart:', error);
      throw error;
    }
  };

  const handleOrder = async (productId, userId) => {
    try {
      const success = await addToCart(productId, userId);
      if (success) {
      }
    } catch (error) {
      console.error('Error placing order:', error);
    }
  };

  return (
    <Container>
      <Row className="p-5 text-center text-dark">
        {products.map((product, index) => (
          <Col key={index} className="mx-auto" xs={12} md={4} lg={3}>
            <Card style={{ width: '18rem' }}>
              <Card.Img variant="center" className="rounded mx-auto d-block" alt="Edit" />
              <Card.Body>
                <Card.Title>{product.name}</Card.Title>
                <Card.Text>Description: {product.description}</Card.Text>
                <Card.Text>Price: ₱{product.price}</Card.Text>
                <Card.Text>Quantity: {product.quantity}</Card.Text>
                {user.role !== 'admin' ? (
                  <Button variant="dark" onClick={() => handleOrder(product._id, user._id)} href="/cart">
                    Buy
                  </Button>
                ) : (
                  <>
                    <Card.Text>Status: {product.status ? 'Active' : 'Inactive'}</Card.Text>
                    <Button variant="dark" href="/editProduct">Edit</Button>
                  </>
                )}
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
  );
}