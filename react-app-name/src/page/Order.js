import { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import UserContext from '../userContext';
import { Button, Card, Col, Row, Container } from 'react-bootstrap';
import { getCartItems, checkoutOrder } from '../api/cart';

export default function Cart() {
  const { user } = useContext(UserContext);
  const [cartItems, setCartItems] = useState([]);

  useEffect(() => {
    fetchCartItems();
  }, []);

  const fetchCartItems = async () => {
    try {
      const response = await getCartItems(user._id);
      setCartItems(response);
    } catch (error) {
      console.error('Error fetching cart items:', error);
    }
  };

  const handleCheckout = async () => {
    try {
      const success = await checkoutOrder(user._id);
      if (success) {
        // Handle success (e.g., show a success message, redirect to a confirmation page)
      }
    } catch (error) {
      console.error('Error checking out order:', error);
    }
  };

  return (
    <Container>
      <Row className="p-5 text-center text-dark">
        {cartItems.map((item, index) => (
          <Col key={index} className="mx-auto" xs={12} md={4} lg={3}>
            <Card style={{ width: '18rem' }}>
              <Card.Img variant="center" className="rounded mx-auto d-block" alt="Product Image" />
              <Card.Body>
                <Card.Title>{item.product.name}</Card.Title>
                <Card.Text>Description: {item.product.description}</Card.Text>
                <Card.Text>Price: ₱{item.product.price}</Card.Text>
                <Card.Text>Quantity: {item.quantity}</Card.Text>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
      <Row className="p-3 justify-content-center">
        <Button variant="dark" onClick={handleCheckout}>Checkout</Button>
      </Row>
    </Container>
  );
}