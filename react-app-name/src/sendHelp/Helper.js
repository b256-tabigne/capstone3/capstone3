export const setUserData = (data) => {
	localStorage.setItem("user", JSON.stringify(data))
}


export const getUserData = () => {
	const user = JSON.parse(localStorage.getItem("user"))
	if (user){
		return user
	}
}

export const deleteUserData = () => {
	    localStorage.clear();
	    return null
	  }
