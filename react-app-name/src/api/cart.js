export const addToCart = async (productId, userId) => {
  try {
    const response = await fetch(`REACT_APP_API_URL=http://localhost:3001/cart/addToCart`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        productId: productId,
        userId: userId,
      }),
    });

    const data = await response.json();
    return data.success;
  } catch (error) {
    console.error('Error adding item to cart:', error);
    throw error;
  }
};

export const getCartItems = async (userId) => {
  try {
    const response = await fetch(`REACT_APP_API_URL=http://localhost:3001/cart/cart`);
    const data = await response.json();
    return data.cartItems;
  } catch (error) {
    console.error('Error fetching cart items:', error);
    throw error;
  }
};

const updateProductQuantity = async (productId, quantity) => {
  try {
    const response = await fetch(`REACT_APP_API_URL=http://localhost:3001/cart/cart`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ quantity }),
    });
    const data = await response.json();
    return data.success;
  } catch (error) {
    console.error('Error updating product quantity:', error);
    throw error;
  }
};

export const checkoutOrder = async (userId) => {
  try {
    const cartItems = await getCartItems(userId);

    for (const cartItem of cartItems) {
      const { productId, quantity } = cartItem;

      const updatedQuantity = cartItem.product.quantity - quantity;

      await updateProductQuantity(productId, updatedQuantity);
    }

    const response = await fetch(`http://localhost:3001/cart/cart`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
    });
    const data = await response.json();
    return data.success;
  } catch (error) {
    console.error('Error checking out order:', error);
    throw error;
  }
};